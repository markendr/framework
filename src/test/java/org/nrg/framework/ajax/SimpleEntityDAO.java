/*
 * framework: org.nrg.framework.ajax.SimpleEntityDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.framework.ajax;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;


@Repository
public class SimpleEntityDAO extends AbstractHibernateDAO<SimpleEntity> {

}
